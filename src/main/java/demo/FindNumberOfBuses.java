package demo;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindNumberOfBuses {

  public void main(String[] args) throws InterruptedException {
    System.out.println("Entering in FindNumberOfBuses");
  }

  public static void testFunction() throws InterruptedException {

    //Starting Point of Execution
    System.out.println("Entering main() in FindNumberOfBuses");

    //Launch Chrome Browser
    WebDriverManager.chromedriver().timeout(30).setup();

    ChromeDriver driver = new ChromeDriver();
    //maximize and Implicit Wait
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    //Launching the URL and maximize
    driver.get("https://www.redbus.in/");

    System.out.println("started launching the url");

    //Entering Source City
    System.out.println("Entering Source City");
    driver.findElementById("src").clear();
    driver.findElementById("src").sendKeys("Bangalore");

    driver.findElementByXPath("//*[@id='search']/div/div[1]/div/ul/li[1]").click();

    //Entering Destination City
    System.out.println("Entering destination City");
    driver.findElementById("dest").clear();
    driver.findElementById("dest").sendKeys("Hyderabad");
    driver.findElementByXPath("//*[@id='search']/div/div[2]/div/ul/li[1]").click();

    //Selecting the Journey Date //td[@class="current day"]
    driver.findElementById("onward_cal").click();

    //driver.findElementByClassName("current day").click();
    driver.findElementByXPath("//td[@class='current day']").click();
    //driver.findElementByXPath("//td[text()='14']").click();
    Thread.sleep(300);
    //Click Search
    driver.findElementById("search_btn").click();

    //Incase network is slow
    Thread.sleep(4000);

    //Get Minimum Fare Details
    System.out.println("Get Minimum Fare Details");
    String minFareDetails = driver.findElementByClassName("fr-strts-frm").getText();
    System.out.println(minFareDetails);

    //Get Minimum Fare Details
    String numberOfBuses = driver.findElementByXPath("//span[text()='Buses']").getText();
    System.out.println(numberOfBuses);
  }
}

